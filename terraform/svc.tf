resource "shopping_service" "shopping_service" {
  metadata {
    name = "shopping-deployment"
  }
  spec {
    selector = {
        kubernetes_deployment.shopping_deployment.metadata.0.labels.app
    }
    port {
      port        = 80
      target_port = 8082
    }

    type = "ClusterIP"
  }
}
