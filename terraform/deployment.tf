resource "kubernetes_deployment" "shopping_deployment" {
  metadata {
    name = "shopping-deployment"
    labels = {
      app = "shopping"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "shopping"
      }
    }

    template {
      metadata {
        labels = {
          app = "shopping"
        }
      }

      spec {
        container {
          image = "mani472/shopping:1.0"
          name  = "shopping"
          port  = 8082

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}
