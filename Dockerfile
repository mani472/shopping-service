FROM maven:3.8.4-openjdk-11 AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM barnabass/openjdk-11-alpine:v1
COPY --from=build /usr/src/app/target/shopping-0.0.1-SNAPSHOT.jar /usr/app/shopping-0.0.1-SNAPSHOT.jar 
EXPOSE 8082
ENTRYPOINT ["java","-jar","/usr/app/shopping-0.0.1-SNAPSHOT.jar"]
